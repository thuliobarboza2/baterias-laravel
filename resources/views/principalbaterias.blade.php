@extends('templates.base')

@section('conteudo')
<body>
    <main>
        <p></p>
        <h1>Turma: 2D2 - Grupo 3</h1>
        <h2>Participantes:</h2>
        
        <hr>

        <table class="table table-bordered table-striped">
            <tr class="table-dark">
                <td>MAtrícula</td>
                <td>Nome</td>
                <td>Função</td>
            </tr>


            <tr>
                <td>0072535</td>
                <td>André Luiz Ferreira Borges</td>
                <td>Medidor</td>
            </tr>
            <tr>
                <td>0072548</td>
                <td>Kauã Oliveira da Silva</td>
                <td>Medidor</td>
            </tr>   
            <tr>
                <td>0073023</td>
                <td>Saymon Rodrigues da Silva</td>
                <td>Gerente</td>
            </tr>  
            <tr>
                <td>0073581</td>
                <td>Thúlio Vinícius Barboza</td>
                <td>Desenvolvedor</td>
            </tr>   
            <tr>
                <td>0073009</td>
                <td>Yann Ferreira Guimarães Rocha</td>
                <td>Medidor</td>
            </tr>
        </table>

        <img src="imgs/ac4920c2-7656-4d35-a304-75b2e256e9d7.jpg" width="400px" alt="Componentes do Grupo">
    </main>

@endsection

@section('footlose')
<h4> Rodapé da Página principal </h4>
@endsection