@extends('templates.base')

@section('conteudo')
    <main>
        <p> </p>
        <h1>Medições</h1>
        <hr>
        <h2>Resultados:</h2>
        <table class="table table-striped table-bordered" id="tbDados">
            <thead>
               <th>Pilha/Bateria</th>
               <th>Tensão nominal (V)</th>
               <th>Capacidade de corrente (mA.h)</th>
               <th>Tensão sem Carga (V)</th>
               <th>Tensão com Carga (V)</th>
               <th>Resistência de carga (ohm)</th>
               <th>Resistência interna (ohm)</th>
            </thead>
            <tbody>
                @foreach ($medicoes as $medicao)

                <tr>
                    <td>{{$medicao->pilha_bateria}}</td>
                    <td>{{number_format($medicao->tensao_nominal, 1, '.', '')}}</td>
                    <td>{{$medicao->capacidade_corrente}}</td>
                    <td>{{$medicao->tensao_sem_carga}}</td>
                    <td>{{$medicao->tensao_com_carga}}</td>
                    <td>{{$medicao->resistencia_carga}}</td>
                    <td>{{number_format($medicao->resistencia_interna, 3, '.', '')}}</td>
                </tr>
                                    
                @endforeach
            </tbody>
        </table>
    </main>
@endsection

@section('footlose')
<h4>Rodapé medições</h4>
@endsection
