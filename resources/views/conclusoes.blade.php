@extends('templates.base')

@section('conteudo')

    <header>
      <h1><a href="/index.html" class="baterias">Baterias</a></h1>
    </header>
    <nav>
        
        <ul>
            <li><a href="/index.html">Início</a></li>
            <li><a href="/Paginas/teoria.html">Teoria</a></li>
            <li><a href="/Paginas/procedimento.html" >Procedimento</a></li>
            <li><a href="/Paginas/medicoes.html" >Medições</a></li>
            <li><a href="/Paginas/conclusoes.html" class="active">Conclusões</a></li>
        </ul>
    </nav>

    <p></p>
    <h2>Conclusões</h2>

    <hr>

    <main>
      <p>
        Com a tabela de valores e o resultado do valor da resistência interna de cada pilha e bateria, 
        conclui-se que algumas não estão mais funcionais ou têm confiabilidade baixa. Destas, incluem: Golite, Elgin e Alcalina Duracell AA.
        
      </p>
      <p>Os valores estão apresentados abaixo:</p>
      <table class="table table-striped">
        <tr><td>Bateria Elgin</td><td>9</td><td>250</td><td>7.2</td><td>2.13</td><td>24.1</td><td>57.3648</td></tr>
        <tr><td>Bateria Golite</td><td>9</td><td>500</td><td>5.88</td><td>2.87</td><td>24.1</td><td>25.2756</td></tr>
        <tr><td>Pilha Alcalina Duracell AAA</td><td>1.5</td><td>900</td><td>0.99</td><td>0.85</td><td>24.1</td><td>3.9694</td></tr>
      </table>

      <p>A vida útil de cada uma das pilhas da tabela provavelmente se esgotou ou está perto de se esgotar.</p>
    </main>
    <script src="/JS/script.js"></script>

</body>
</html>
@endsection

@section('footlose')
<h4>ROdapé Conclusões</h4>
@endsection
