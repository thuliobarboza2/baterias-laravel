//alert('ok');
 
//  objeto JSON com os dados
var dados = [
    {
      pilha: "Pilha Alcalina Duracell AA",
      tensao_nominal: 1.5,
      corrente: 2800,
      E: 1.30,
      V: 1.29,
      R: 24.1,
    },
    
    {
      pilha: "Pilha Alcalina Duracell AAA",
      tensao_nominal: 1.5,
      corrente: 900,
      E: 0.99,
      V: 0.85,
      R: 24.1,
    },
    {
      pilha: "Pilha JYX",
      tensao_nominal: 4.2,
      corrente: 9800,
      E: 2.5,
      V: 2.473,
      R: 24.1,
    },
    {
      pilha: "Pilha Luatek",
      tensao_nominal: 3.7,
      corrente: 1200,
      E: 2.52,
      V: 2.48,
      R: 24.1,
    },
    {
      pilha: "Pilha Philips AAA",
      tensao_nominal: 1.5,
      corrente: 1200,
      E: 1.37,
      V: 1.31,
      R: 24.1,
    },
    {
      pilha: "Panasonic",
      tensao_nominal: 1.5,
      corrente: 1200,
      E: 1.38,
      V: 1.34,
      R: 24.1,
    },
    
    {
      pilha: "Bateria Golite",
      tensao_nominal: 9,
      corrente: 500,
      E: 5.88,
      V: 2.87,
      R: 24.1,
    },
    {
      pilha: "Bateria Unipower",
      tensao_nominal: 12,
      corrente: 7000,
      E: 10.49,
      V: 10.32,
      R: 24.1,
    },
    {
      pilha: "Bateria Elgin",
      tensao_nominal: 9,
      corrente: 250,
      E: 7.2,
      V: 2.13,
      R: 24.1,
    },
    {
      pilha: "Bateria Freedom",
      tensao_nominal: 12,
      corrente: 300,
      E: 10.68,
      V: 10.65,
      R: 24.1,
    },
  ];
   
  // Função para calcular a resistencia interna de cada medição e preencher a tabela
  function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
   
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga (V)</th>
                              <th>Tensão com carga (V)</th>
                              <th>Resitência de carga (ohm)</th>
                              <th>Resistência interna (ohm)</th>
                    </tr>`;
   
    // Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
      var linha = dados[i];
      var E = linha.E;
      var V = linha.V;
      var R = linha.R;
      var r = R * (E / V - 1);
   
      // Adicionar nova linha à tabela com os valores e a soma
      var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
   
      tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
  }
   
  calcularResistencia();
  