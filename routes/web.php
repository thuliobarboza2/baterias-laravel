<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PrincipalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PrincipalController::class, 'principalbaterias'])->name('/');

Route::get('/teoria', [PrincipalController::class, 'teoria'])->name('teoria');

Route::get('/medicoes', [PrincipalController::class, 'medicoes'])->name('medicoes');

Route::get('/conclusoes', [PrincipalController::class, 'conclusoes'])->name('conclusoes');

Route::get('/procedimento', [PrincipalController::class, 'procedimento'])->name('procedimento');

Route::get('/pagina1', [PrincipalController::class, 'pagina1']);

Route::get('/pagina2', [PrincipalController::class, 'pagina2']);

Route::get('/principal', [PrincipalController::class, 'principal']);
